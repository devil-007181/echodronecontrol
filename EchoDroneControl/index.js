/**
 * Control your APM quadcopter with your voice, using Amazon Alexa, Lambda, Tlemetry MQTT.
 */
 
/**
 * Globle variable.
 */
var awsIot = require('aws-iot-device-sdk');
var config = require("./config");
var deviceName = "ARVIS_Yaan";        // this device is really the controller
var mqtt_config = {
                   privateKey: config.privateKey,
                   clientCert: config.certificate,
                   caCert: config.rootCA,
                   host: config.host,
                   port: 8883,
                   clientId: deviceName,  
                   region: "ap-south-1",
                   debug: true
                 };
var ctx = null;
var client = null;






/**
 * Route the incoming request based on type (LaunchRequest, IntentRequest, etc.)
 *The JSON body of the request is provided in the event parameter.
 */
exports.handler = function (event, context) 
   {
     try 
       {
           
         //check alexa id is true then session started
         ctx = context;
         console.log("event.session.application.applicationId=" + event.session.application.applicationId);
         console.log(event);
         console.log(ctx);
         if (event.session.application.applicationId !== "amzn1.ask.skill.815509f4-e7f6-40a3-a902-cd3fd6d6bd78")
           {
            ctx.fail("Invalid Application ID");
           }
         
         // connection of AWS_IOT
         client = awsIot.device(mqtt_config);
         client.on ("connect",function()
           {
             console.log("Connected to AWS IoT");
            });
            
           if (event.session.new) 
           {
               onSessionStarted({requestId: event.request.requestId}, event.session);
            }
          if (event.request.type === "LaunchRequest") 
           {
              onLaunch(event.request, event.session);
            }
            
          else if (event.request.type === "IntentRequest") 
           {
              onIntent(event.request, event.session);
            } 
         
           else if (event.request.type === "SessionEndedRequest") 
           {
             onSessionEnded(event.request, event.session);
             ctx.succeed();
            }
        }      
       catch (e) 
       {
           console.log("EXCEPTION in handler:  " + e);
           ctx.fail("Exception: " + e);
        }
    };








/** 
 * Called when the session starts.
 */
function onSessionStarted(sessionStartedRequest, session) 
   {
      console.log("onSessionStarted requestId=" + sessionStartedRequest.requestId + ", sessionId=" + session.sessionId);
    }



/**
 * Called when the user launches the skill without specifying what they want.
 */
function onLaunch(launchRequest, session, callback) 
    {
      console.log("onLaunch requestId=" + launchRequest.requestId + ", sessionId=" + session.sessionId);
      
      // Dispatch to your skill's launch.
      getWelcomeResponse(callback);
    }

    

/**
 * Called when the user specifies an intent for this skill.
 */
function onIntent(intentRequest, session ) {
       console.log('onIntent requestId=${intentRequest.requestId}, sessionId= ${session.sessionId}');
       
       var intent = intentRequest.intent;
       var intentName = intentRequest.intent.name;
       
       console.log("REQUEST to string =" + JSON.stringify(intentRequest));
       
       var callback = "";
       
      // Dispatch to your skill's intent handlers 
      if ( intentName  === "Gointent" )
       {
          doGoIntent(intent, session);
        } 
       else if ( intentName === "Commandintent" ) 
       {
         doCommandIntent(intent, session);
        }     
       else if ( intentName === "Turnintent" ) 
       {
          doTurnIntent(intent, session);
        } 
        else if ("AMAZON.HelpIntent" === intentName) 
        {
          getWelcomeResponse();
        } 
        else if ( intentName === "AMAZON.StopIntent" )
       {
          onSessionEnded(session, callback);
        } 
        else if (intentName === "AMAZON.CancelIntent")
       {
          onSessionEnded(session, callback);
        } 
        else if ("AMAZON.MoreIntent" === intentName)
        {
          getWelcomeResponse();
        }
        else if ("AMAZON.NavigateHomeIntent" === intentName)
        {
          getWelcomeResponse();
        }
        else if ("AMAZON.NavigateSettingsIntent" === intentName)
        {
            getWelcomeResponse();
        }
        else if ("AMAZON.NextIntent" === intentName)
        {
            getWelcomeResponse();
        }
        else if ("AMAZON.PageUpIntent" === intentName)
        {
            getWelcomeResponse();
        }
        else if ("AMAZON.PageDownIntent" === intentName)
        {
            getWelcomeResponse();
        }
        else if ("AMAZON.PreviousIntent" === intentName)
         {
            getWelcomeResponse();
        }
        else if ("AMAZON.ScrollRightIntent" === intentName)
        {
            getWelcomeResponse();
        }
        else if ("AMAZON.ScrollDownIntent" === intentName)
        {
            getWelcomeResponse();
        }
        else if ("AMAZON.ScrollLeftIntent" === intentName)
        {
            getWelcomeResponse();
        }
        else if ("AMAZON.ScrollUpIntent" === intentName)
        {
            getWelcomeResponse();
        }
        else if ("AMAZON.PauseIntent" === intentName)
        {
            getWelcomeResponse();
        }
        else if ("AMAZON.ResumeIntent" === intentName) 
        {
            getWelcomeResponse();  
        }
        else 
       {
         throw "invalid intent";
        }
        
    }
    
   
    
/**
 * Called when the session ends.
 */
function onSessionEnded(sessionEndedRequest, session, callback ) {
    console.log(`onSessionEnded requestId=${sessionEndedRequest.requestId}, sessionId=${session.sessionId}`);
   // Add cleanup logic here  
             var sessionAttributes = {};
             var cardTitle = 'Session Ended';
             var speechOutput = 'Thank you for using ARVIS, have a great day!';
             var shouldEndSession = true;
             ctx.succeed(buildResponse(sessionAttributes, buildSpeechletResponse(cardTitle, speechOutput, null, shouldEndSession,  )));
    }    














    
// --------------- Functions that control the skill's behavior -----------------------

function getWelcomeResponse() 
    {
      // If we wanted to initialize the session to have some attributes we could add those here.
      var sessionAttributes = {};
      var cardTitle = "Welcome";
      var speechOutput = "Welcome to the ARVIS Yaan....";
      
      // TODO:  is drone online or offline?  If online, is it ARMED?
      
      var repromptText = "ARVIS is ready for Command...Please, tell me a command.";
      var shouldEndSession = false;
      
      ctx.succeed(buildResponse(sessionAttributes, buildSpeechletResponse(cardTitle, speechOutput, repromptText, shouldEndSession)));
    }
    
    


/**
 *  handles GO intent.
 */
function doGoIntent(intent, session, callback) 
   {
      var cardTitle = "Tell me how far to go and in what direction...";
      var repromptText = "";
      var sessionAttributes = {};
      var speechOutput = "";
      var shouldEndSession = false;
      
      var direction = intent.slots.Direction.value;
      var distance = intent.slots.Distance.value; 
      var unit = intent.slots.Unit.value;
      
      var validDirections = ["downward", "down", "upward", "up", "right", "left", "backward", "back", "straight", "forward", "front", "ahead", "northeast", "northwest", "southeast", "southwest", "south", "north", "west", "east"];
      var validUnits = ["meter", "feet" ];
      
      repromptText = "Tell me how far to go and in what direction.  ";
      var fail = false;
      
      // validate inputs
     if ( !( parseInt(distance,10) >= 1 )  )
        {
          speechOutput = "I couldn't understand the distance you want me to travel. ";
          fail = true;
          ctx.succeed(buildResponse(sessionAttributes, buildSpeechletResponse(cardTitle, speechOutput, repromptText, shouldEndSession)));
        }
     if (validDirections.indexOf(direction) == -1)
        {
          speechOutput = "I couldn't understand the direction you want me to travel. ";
          fail = true;
          ctx.succeed(buildResponse(sessionAttributes, buildSpeechletResponse(cardTitle, speechOutput, repromptText, shouldEndSession)));
        }
      if (validUnits.indexOf(unit) == -1)
        {
          speechOutput = "I couldn't understand the unit you want me to travel.  ";
          fail = true;
          ctx.succeed(buildResponse(sessionAttributes, buildSpeechletResponse(cardTitle, speechOutput, repromptText, shouldEndSession)));
       }
       if(!fail)
        {
          cardTitle = "ARVIS_Yaan is going" + " " + direction + " " + distance + " " + unit;
          speechOutput = "going" + " " + direction + " " + distance + " " + unit;
          ctx.succeed(buildResponse(sessionAttributes, buildSpeechletResponse(cardTitle, speechOutput, repromptText, shouldEndSession)));
          mqttPublish(intent, sessionAttributes, cardTitle, speechOutput, repromptText, shouldEndSession);
        }
    }




function doCommandIntent(intent, session, callback) 
   {
       var cardTitle = "Tell me what is the command for the ARVIS" ;
       var repromptText = null;
       var sessionAttributes = {};
       var speechOutput = "";
       var shouldEndSession = false;
       
       repromptText = "Tell me what is the command for the ARVIS.  ";
       
       var task = intent.slots.Task.value;
       var validTasks = [ "rtl", "return to launch", "land", "stand", "stay", "hold",  "take off", "launch" ];
       
       if (validTasks.indexOf(task) == -1)
        {
          speechOutput = "I couldn't understand the command.  ";
          ctx.succeed(buildResponse(sessionAttributes, buildSpeechletResponse(cardTitle, speechOutput, repromptText, shouldEndSession)));
        }
      else
        {
          cardTitle = "Executing ARVIS command " + task ;
          speechOutput = "executing command" + " " + task;
          ctx.succeed(buildResponse(sessionAttributes, buildSpeechletResponse(cardTitle, speechOutput, repromptText, shouldEndSession)));
          mqttPublish(intent, sessionAttributes, cardTitle, speechOutput, repromptText, shouldEndSession);
        }
    }




function doTurnIntent(intent, session, callback) 
    {
      var cardTitle = "Tell me how much you want to turn the ARVIS Yaan." ;
      var repromptText = null;
      var sessionAttributes = {};
      var speechOutput = "";
      var shouldEndSession = false;
      
      repromptText = "Tell me how much you want to turn the ARVIS Yaan.  ";
      
      var direction =  intent.slots.Direction.value;
      var distance = intent.slots.Distance.value;
      var unit = intent.slots.Unit.value;
      
      var validDirections = ["right", "left", "backward", "back", "straight", "forward", "front", "ahead", "northeast", "northwest", "southeast", "southwest", "south", "north", "west", "east"];
      var validUnits = ["degree"];
      
      var fail = false;
      
      if ( !( parseInt(distance,10) >= 1 )  )
        {
          speechOutput = "I couldn't understand the distance you want me to turn. ";
          fail = true;
          ctx.succeed(buildResponse(sessionAttributes, buildSpeechletResponse(cardTitle, speechOutput, repromptText, shouldEndSession)));
        }
      if (validDirections.indexOf(direction) == -1)
        {
          speechOutput = "I couldn't understand the direction of the turn. ";
          fail = true;
          ctx.succeed(buildResponse(sessionAttributes, buildSpeechletResponse(cardTitle, speechOutput, repromptText, shouldEndSession)));
        }
        if (validUnits.indexOf(unit) == -1)
        {
          speechOutput = "I couldn't understand the unit you want me to turn.  ";
          fail = true;
          ctx.succeed(buildResponse(sessionAttributes, buildSpeechletResponse(cardTitle, speechOutput, repromptText, shouldEndSession)));
       }
       if(!fail) 
        {
           cardTitle = "ARVIS Yaan turning" + " " + direction + " " + distance + " " + unit;
          speechOutput = "turning " + " " + direction + " " + distance + " " + unit;
          ctx.succeed(buildResponse(sessionAttributes, buildSpeechletResponse(cardTitle, speechOutput, repromptText, shouldEndSession)));
          mqttPublish(intent, sessionAttributes, cardTitle, speechOutput, repromptText, shouldEndSession);
        }
    }



function mqttPublish(intent, sessionAttributes, cardTitle, speechOutput, repromptText, shouldEndSession)
    {
      var strIntent = JSON.stringify(intent);
      console.log("mqttPublish:  INTENT text = " + strIntent);
      //    client.publish("ikw1zr46p50f81z/drone/echo", strIntent, false);
      client.publish(config.topic, strIntent, false);
      client.end();
      
      client.on("close", (function () 
        {
          console.log("MQTT CLIENT CLOSE - thinks it's done, successfully. ");
          ctx.succeed(buildResponse(sessionAttributes, buildSpeechletResponse(cardTitle, speechOutput, repromptText, shouldEndSession)));
        }));
        
      client.on("error", (function (err, granted) 
        {
          console.log("MQTT CLIENT ERROR!!  " + err);
        }));
    }
    
    
    

    
    
    


// --------------- Helpers that build all of the responses -----------------------

function buildSpeechletResponse(title, output, repromptText, shouldEndSession) 
   {
     return {
            outputSpeech: {
            type: "PlainText",
            text: output,
            },
            card: {
            type: "Simple",
            title: title,
            content: output,
            },
            reprompt: {
            outputSpeech: {
            type: "PlainText",
            text: repromptText,
            }
            },
            shouldEndSession,
        };
    }

function buildResponse(sessionAttributes, speechletResponse)
   {
      return{
           version: "1.0",
           sessionAttributes: sessionAttributes,
           response: speechletResponse,
       };
    }